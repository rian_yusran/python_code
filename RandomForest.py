import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

df_training = pd.read_csv("./train.csv")
df_testing = pd.read_csv("./test.csv")

#choose training features
features = ['Pclass','Sex','SibSp','Parch']
#drop another feature, and do ONE HOT encoding
X = pd.get_dummies(df_training[features])
y = df_training['Survived']

#testing
X_test = pd.get_dummies(df_testing[features])

model = RandomForestClassifier(n_estimators=100, max_depth=5, random_state=1)
model.fit(X, y)
y_train_predicted = model.predict(X)

print('The accuracy of the Random Forests model is :\t',metrics.accuracy_score(y_train_predicted,y))

predictions = model.predict(X_test)
output = pd.DataFrame({'PassengerId': df_testing.PassengerId, 'Survived':predictions})
output.to_csv('./submission1.csv', index=False)
print("Your submission was successfully saved!")